---
title:  'Process: Ask for an VPS Instance'
subtitle: "M254 LB3 Dokumentation"
author:
- Luca Suter
- Valentin Binotto
description: |
    Dieses Dokument beschreibt den Geschäftsprozess, eine VPS Instanz in der Hetzner-Cloud eines Unternehmens anzufordern bzw. zu erstellen.
    Weiter wird in dieser Schrift die Arbeit an diesem Prozess im Rahmen der LB3 im Modul 254 dokumentiert.
numbersections: true
urlcolor: blue
header-includes: |
  \usepackage{fancyhdr}
  \pagestyle{fancy}
  \lfoot{}
  \cfoot{}
  \rfoot{Seite \thepage}
  \rhead{Binotto, Suter}
  \chead{Process: Get an VPS Instance}
  \lhead{TBZ M254}
geometry: margin=2cm
...




\newpage{}
\tableofcontents
\newpage{}
# Einleitung
Dieses Dokument beschreibt den Geschäftsprozess, eine VPS Instanz in der Hetzner-Cloud eines Unternehmens anzufordern bzw. zu erstellen.

Weiter wird in dieser Schrift die Arbeit an diesem Prozess im Rahmen der LB3 im Modul 254 dokumentiert.

## Auftrag LB3
Im Rahmen der LB3 im Modul 254, wurde uns, den Autoren, der Auftrag erteilt, selbstständig ein Projekt im Zusammenhang mti Geschäftsprozessen durchzuführen. Im Rahmen dieses Projekts sollen sich die Autoren näher mit Prozess Engines (z.B. Camunda) und BPMN allgemein auseinandersetzen.

Wir haben uns dazu entschlossen, einen Geschäftsprozess zu beschreiben sowie diesen mithilfe von Camunda 7 umzusetzen. Wahlweise bietet sich uns die Möglichkeit im Laufe dieser Projektarbeit, diese um weitere Elemente zu erweitern, also beispielsweise die Plattform von Hetzner direkt zu integrieren.

Wir möchten im Rahmen dieses Projekts uns im Detail mit den Möglichkeiten von Camunda 7 auseinandersetzen. Insbesondere im Hinblick auf die Nutzung von Formularen, automatisierten Genehmigungen sowie externe Schnittstellen (wie das Ansprechen externer APIs (z.B. Hetzner REST/HTTP API)).

## Geschäftsprozess
Der Geschäftsprozess, welchen wir in dieser Schrift beschreiben, hat zum Ziel, die Anforderung und Erstellung von Virtual Private Server (VPS) Instanzen in der Hetzner Cloud eines fiktiven Unternehmens (AAL), zu vereinfachen und soweit als möglich zu automatisieren. Wir behalten uns vor den Umfang zu erweitern, indem wir weiter Hetzner Ressourcen automatisiert einbinden können.

Nachfolgend möchten wir im Detail auf diesen Prozess eingehen:

### Beteiligte Personen/Parteien

**Antliuma Aerospace Laboratories (AAL)** \newline{}
Unternehmen, welchem dieser Geschäftsprozess zugeordnet ist. Nutzt die Hetzner Cloud für die eigene Infrastruktur.

**Hetzner** \newline{}
Dienstleister, welcher Public Cloud Dienste der Öffentlichkeit und damit auch AAL zur Verfügung stellt.

**AAL - Compliance Abteilung** \newline{}
Genehmigende Partei, bei welcher das Aufsetzen einer VPS Instanz beantragt und genehmigt werden muss.

**AAL - Hetzner Organisation Administrator** \newline{}
Verwaltet alle root Konten in der Hetzner Organisation der AAL.

**Kunde** \newline{}
Bestellt in erster Linie VPS Instanzen. Wie oben erwähnt kann das Angebot erweitert werden. Ein Mitarbeiter der AAL kann auch Kunde sein.

### Ziel des Prozesses
Im Grundsatz ist das Ziel, dass ein Kunde/Mitarbeiter von AAL möglichst schnell und effizient einen Virtuellen Server, nach seinen Anforderungen zur Verfügung hat um diesen dann für seine Zwecke zu nutzen (beispielsweise Bereitstellen einer Webseite, Applikation, Kennenlernen eines GNU/Linux-basierten Betriebssystems etc). In diesem Prozess wollen wir das so weit wie möglich automatisieren und vereinfachen.

### Prozessablauf
Ein Kunde braucht einen virtuellen Server und füllt ein Formular aus, in welchem er die Anforderungen an den Server, den Verwendungszweck und Informationen zu seiner Person angibt. Gewisse Angaben wie der Name der Instanz dürfen frei durch den Nutzer gewählt werden hingegen bei die Auswahl von Systemleistung wird die Auswahl eingeschränkt und dem Nutzer stehen einige Optionen zur Auswahl, aus welchen er eine auswählen muss. Das ausgefüllte Formular wird an die Compliance Abteilung von AAL weitergeleitet, welche dann beurteilt ob die Anfrage im Verhältnis zu den Anforderungen des Kunden stehen und ob die Vorschriften von AAL etc. eingehalten wurden. Der Einfachheit halber wird in diesem Prozess jeder Antrag nur mit einer Ja/Nein Frage beantwortet, in Realität wurde die Compliance Abteilung weiterführende Abklärungen treffen und dem Entscheid eine Begründung beilegen. Wenn der Antrag angenommen wird, erstellt eine Aktivität automatisch anhand der Informationen aus dem Formular eine VPS Instanz. Die Daten, welche der Kunde im Antragsformular zur Instanz eingegeben hat, werden via REST API an Hetzner gesendet, wo dann die Instanz gestartet wird, und die Informationen zur erstellten Instanz (z.B. öffentliche IP-Adresse, Instanz-ID etc.) zurück an den AAL Hetzner Admin via Mail gesendet werden (Dieser Ablauf auf Seiten Hetzner ist in einem Subprozess zusammengefasst, wir verzichten hier darauf diesen Subprozess genauer zu definieren). Dieser gibt die Informationen an den Kunden weiter, welche mit der Nutzung der VPS-Instanz direkt beginnen kann.

# Vorgehen und Umsetzung

## Recherche / Informationssuche
Bei der Recherche, wie aus Camunda heraus API-Anfragen an externe Dienste gemacht werden können, sind wir über viel JavaScript gestolpert und zum Schluss auf der Camunda Cloud gelandet. Camunda Cloud verfügt über Plugins, mit welchen man an unterschiedlichste Dienste, wie AWS aber auch Azure, WhatsApp oder Teams direkt API-Request schicken kann. Wir haben uns für die generische REST API outbound Integration mit der FEEL (Friendly Enough Expression Language) können nun API-Queries zusammengebaut werden, so das aus z.B. Formularen mit Variablen dynamische Anfragen generiert werden. Die Antworten (von Hetzner in unserem Falle) werden dann von Camunda wieder empfangen und können so weiteren Aktivitäten übergeben werden.

## Erstellung Model
Die Erstellung von unserem Prozess Modell ist relativ simpel, bis auf den Punkt vom API-Request an Hetzner. Wir haben drei Pools, die die drei Instanzen Kunde, AAL und Hetzner als unseren Dienstleister darstellen. An und für sich ist der Prozess relativ einfach: Der Kunde füllt ein Formular aus, welches an die Compliance Abteilung der AAL geschickt wird. Diese beurteilt den Antrag und gibt eine Ja/Nein Antwort worauf hin bei einem positiven Entscheid die EC2-Instanz via REST-API Aktivität oder System-Aktivität erstellt wird (REST-API Anfrage von Hetzner Root Admin bei AAL zu Hetzner / Nachrichtenfluss) und die Angaben zur neu erstellten Instanz von Hetzner zurück an den Kunden kaskadiert wird. 

Siehe Seite 5:

![Prozess-AWS](img/aws-prozess-lb3.png)

## Erstellung Formular
Das Formular ist beliebig erweiterbar, da VPS-Instanzen beliebig kompliziert sein können. Gewisse Angaben benötigen wir jedoch zwingend (z.B. Instanz-Typ). Wir haben uns auf die wichtigsten Angaben beschränkt. Das Formular kann dann über die Formular-ID direkt mit der Nutzer-Aktivität "Antrag ausfüllen" verknüpft werden, und die Werte, welcher der Nutzer in das Formular eingibt, werden in Variablen gespeichert. Diese können dann von der Aktivität, welcher den API-Call an Hetzner macht abgegriffen werden.

Siehe Seite 6:

![Form-Instanz](img/aws-form.png)
![Dashboard Hetzner](img/hetzner-dash.png)
![Query Camunda](img/query.png)
![REST activity](img/rest-activity.png)

## Probleme / Herausforderungen
Wir sind auf ein grosses Problem gestossen bei der Verwendung unseres Antragsformulars für den Kunde: In der Camunda Cloud haben wir einen Bug gefunden, welcher das Ausfüllen des Formulars übersprang. Sobald der Sequenzfluss gestartet wurde, das Token also im Pool des Kunden erstellt wurde, überspringt Camunda aus unerklärlichen Gründen die Aktivität "Antrag ausfüllen" und damit auch das dazugehörige Formular, in welchem wir die Angaben vom Kunden zu seiner Instanz abholen. Das heisst die benötigten Variablen sind immer Leer an die Aktivität, welche die REST-API-Anfrage absetzt, geliefert worden. Wir haben uns dann in Rücksprache mit Herr Kälin dazu entschieden, dass wir die Daten einfach statisch in der REST-Aktivität festlegen, da es in unserem Projekt nicht primär um Formulare ging sondern um das Ansprechen von externen API's.

# Erkenntnisse

Wir haben gelernt, dass sich in der Camunda Cloud das Absetzen von API-Requests, aufgrund von vorhandenen Integrationen, sehr einfach gestaltet. Anhand des Beispiels mit einer VPS-Instanz haben wir ein relativ komplexes Thema angeschnitten, das Verwenden von APIs zum Erstellen von Cloud-Ressourcen, welches durch die Automation solcher Anfragen wesentlich vereinfacht werden kann. In der heutigen Welt, in welcher beinahe jeder Server und Dienst im öffentlichen Netz steht, ist die Einbindung von API's ein äusserst mächtiges Instrument. Es können Nachrichten per Teams oder WhatsApp versendet werden, wenn ein Task abgeschlossen ist, oder um Informationen an den Nutzer zu übertragen, mit der Anbindung von Public-Cloud-Anbietern wie AWS oder Azure können Server, Speicher, Applikationen automatisiert gestartet, gestoppt oder dem Bedarf nach skaliert werden. Dies birgt unzählige Möglichkeiten, wie man Arbeiten automatisiert, effizient und ressourcenarm erledigen kann.

